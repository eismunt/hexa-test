import './styles.scss';

function ToggleNav() {
  const header = document.querySelector('.header-bottom');
  const button = header.querySelector('.header-bottom__menu-btn');
  // const nav = header.querySelector('.Header__menu');

  if (!header && !button) return;

  this.state = {
    isOpen: false,
  };
  this.sel = header;
  const showEvent = new CustomEvent('show');

  this.show = () => {
    if (this.state.isOpen) return;

    this.state.isOpen = true;
    header.classList.add('header-bottom--open');
    this.sel.dispatchEvent(showEvent);
  };

  this.hide = () => {
    if (!this.state.isOpen) return;

    this.state.isOpen = false;
    header.classList.remove('header-bottom--open');
  };

  button.addEventListener('click', () => (this.state.isOpen ? this.hide() : this.show()), false);

  window.addEventListener('orientationchange', this.hide);
}

window.addEventListener('DOMContentLoaded', () => {
  const toggleNav = new ToggleNav();

  toggleNav.sel.addEventListener('show', () => {});
});